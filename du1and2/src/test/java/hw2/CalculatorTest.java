package hw2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void add() {
        Calculator test = new Calculator();

        int expected1 = 3;
        int expected2 = 1;

        int res1 = test.add(1, 2);
        int res2 = test.add(-1, 2);

        Assertions.assertEquals(expected1, res1);
        Assertions.assertEquals(expected2, res2);

    }

    @Test
    void subtract() {
        Calculator test = new Calculator();

        int expected1 = 1;
        int expected2 = -3;

        int res1 = test.subtract(2, 1);
        int res2 = test.subtract(-1, 2);

        Assertions.assertEquals(expected1, res1);
        Assertions.assertEquals(expected2, res2);

    }

    @Test
    void multiply() {
        Calculator test = new Calculator();

        int expected1 = 2;
        int expected2 = 0;
        int expected3 = -5;

        int res1 = test.multiply(2, 1);
        int res2 = test.multiply(-1, 0);
        int res3 = test.multiply(-1, 5);

        Assertions.assertEquals(expected1, res1);
        Assertions.assertEquals(expected2, res2);
        Assertions.assertEquals(expected3, res3);

    }

    @Test
    void divide() throws Exception {
        Calculator test = new Calculator();

        int expected1 = 2;
        int expected2 = 0;
        int expected3 = -1;
        int expected4 = 0;

        int res1 = test.divide(2, 1);
        int res2 = test.divide(2, 3);
        int res3 = test.divide(2, -2);
        int res4 = test.divide(0, 5);

        Assertions.assertEquals(expected1, res1);
        Assertions.assertEquals(expected2, res2);
        Assertions.assertEquals(expected3, res3);
        Assertions.assertEquals(expected4, res4);

        Exception exception = Assertions.assertThrows(Exception.class, () -> test.divide(1, 0));

        String expectedMessage = "Ocekavana vyjimka";
        String actualMessage = exception.getMessage();

        Assertions.assertEquals(expectedMessage, actualMessage);

    }
}