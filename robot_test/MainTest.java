package org.tutorial;

import static org.junit.jupiter.api.Assertions.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;


class MainTest {
    private WebDriver driver;
    private String url = "https://moodle.fel.cvut.cz/mod/quiz/view.php?id=265999";

    @BeforeEach
    public void setUp() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--whitelisted-ips=\"\"");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(options);
        driver.get(url);
        driver.manage().window().maximize();
    }



    @Test
    public void testLogin() {
        driver.get("https://moodle.fel.cvut.cz/mod/quiz/view.php?id=265999");

        driver.findElement(By.xpath("//*[@id=\"sso-form\"]/a")).click();


        // Enter login email and password
        driver.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys("username");
        driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("password");


        // submit login and password
        driver.findElement(By.xpath("//*[@id=\"cvut-login-form\"]/form/fieldset/div/div/div/div[5]/button")).click();


        // select Test Robot
        WebElement button = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section/div[2]/div[5]/div/form/button"));
        button.click();


        // zahajeni pokusu
        // start Test Robot
        driver.findElement(By.xpath("//*[@id=\"id_submitbutton\"]")).click();
        //*[@id="id_submitbutton"]


        WebElement textarea =
                driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section[1]/div[2]/form/div/div[1]/div[2]/div/div[2]/div[1]/textarea"));
        textarea.click();
        textarea.sendKeys("Vladyslav Hordiienko 101");


        WebElement time =
                driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section[1]/div[2]/form/div/div[2]/div[2]/div[1]/div[2]/span/input"));
        time.click();
        time.sendKeys("86400");


        WebElement selectPlanet =
                driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section[1]/div[2]/form/div/div[3]/div[2]/div[1]/div/p/span/select"));
        Select select1 = new Select(selectPlanet);
        select1.selectByVisibleText("Oberon");


        WebElement selectCountry =
                driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section[1]/div[2]/form/div/div[4]/div[2]/div[1]/div/p/span/select"));
        Select select2 = new Select(selectCountry);
        select2.selectByVisibleText("Rumunsko");


        WebElement endButton =
                driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section[1]/div[2]/form/div/div[5]/input"));
        endButton.click();

        WebElement log_out =
                driver.findElement(By.xpath("//*[@id=\"action-menu-toggle-1\"]"));
        log_out.click();
        WebElement confirm_log_out =
                driver.findElement(By.xpath("//*[@id=\"action-menu-1-menu\"]/a[6]"));
        confirm_log_out.click();

        WebElement confirm2_log_out =
                driver.findElement(By.xpath("//*[@id=\"single_button6470c8c6878315\"]"));
        confirm2_log_out.click();


    }
    @AfterEach
    public void tearDown() {
        driver.quit();
    }



}
