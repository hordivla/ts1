package shop;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {
    int id;
    String name;
    float price;
    String category;
    int loyaltyPoints;
    StandardItem si;
    @BeforeEach
    void init(){
        id = 1;
        name = "Teddy";
        price = 10000;
        category = "Some category";
        loyaltyPoints = 0;
        si = new StandardItem(id,name,price,category,loyaltyPoints);
    }
    @Test
    void standardItemTest(){

        assertEquals(1, si.getID());
        assertEquals("Teddy", si.getName());
        assertEquals(10000, si.getPrice());
        assertEquals("Some category", si.getCategory());
        assertEquals(0, si.getLoyaltyPoints());
    }
    @Test
    void copyTest() {
        StandardItem si_copy = si.copy();

        assertNotNull(si_copy);
        assertEquals(si, si_copy);
        assertEquals(si.getID(), si_copy.getID());
    }
    @Test
    void equalsTest() {
        StandardItem si2 = new StandardItem(2, "Teddy", 10000, "TEXT", 0);
        StandardItem si3 = new StandardItem(1, "Teddy", 10000, "Some category", 0);
        assertEquals(false, si.equals("asd"));
        assertEquals(false, si.equals(si2));
        assertEquals(true, si.equals(si3));
    }




 


    @Test
    public void setIDTest() {
        int id = 100;

        si.setID(id);
        assertEquals(si.getID(), id);
    }

    @Test
    public void setNameTest() {
        String name = "Bear";

        si.setName(name);
        assertEquals(si.getName(), name);
    }

    @Test
    public void setCategoryTest() {
        String category = "animals";

        si.setCategory(category);
        assertEquals(si.getCategory(), category);
    }

    @Test
    public void setPriceTest() {
        int newPrice = 1000;

        si.setPrice(newPrice);
        assertEquals(si.getPrice(), newPrice);
    }

    @Test
    public void setLoyaltyPointsTest() {
        StandardItem item = new StandardItem(3, "cola", 50.0f, "drinks", 0);
        int newPoints = 431;

        item.setLoyaltyPoints(newPoints);
        assertEquals(item.getLoyaltyPoints(), newPoints);
    }

    @Test
    public void toStringTest() {
        int id = 123;
        String name = "me";
        float price = 321.2f;
        String category = "ich";
        int points = 1212;

        StandardItem item = new StandardItem(id, name, price, category, points);

        assertEquals("Item   ID "+id+"   NAME "+name+"   CATEGORY "+category + "   PRICE "+price+"   LOYALTY POINTS "+points, item.toString());
    }

}