package shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {
    ShoppingCart cart = new ShoppingCart();
    StandardItem item1;
    StandardItem item2;
    @BeforeEach
    public void setUp(){
        item1 = new StandardItem(1, "Teddy", 123.456f, "People", 100);
        item2 = new StandardItem(2, "Bear", 111, "Animal", 10);

        cart.addItem(item1);
        cart.addItem(item2);
    }


    @Test
    public void OrderWithStateTest() {
        String name = "Jmeno";
        String address = "Adress";
        int state = 777;

        shop.Order order = new shop.Order(cart, name, address, state);

        assertEquals(cart.getCartItems(),order.getItems());
        assertEquals(name, order.getCustomerName());
        assertEquals(address,order.getCustomerAddress());
        assertEquals(state,order.getState());

    }

    @Test
    public void OrderWithNoStateTest(){
        String name = "Petr Pavel";
        String address = "Prague";

        shop.Order order = new shop.Order(cart, name, address);

        assertEquals(0, order.getState());
        assertEquals(cart.getCartItems(),order.getItems());

    }

    @Test
    public void setItemsTest(){

        shop.Order order = new shop.Order(cart, "Abdul Ahaib", "Pakistan", 3);

        ArrayList<Item> items = new ArrayList<>();
        StandardItem item3 = new StandardItem(3, "AK", 1000, "Weapon", 100);
        items.add(item3);
        items.add(item1);

        order.setItems(items);

        assertEquals(items, order.getItems());
    }

    @Test
    public void setNameAdressStateTest(){

        shop.Order order = new shop.Order(cart, "Snoop", "LA");

        String newName = "Dog";
        String newAddress = "NYC";
        int newState = 50;

        order.setCustomerName(newName);
        order.setCustomerAddress(newAddress);
        order.setState(newState);

        assertEquals(newName, order.getCustomerName());
        assertEquals(newAddress, order.getCustomerAddress());
        assertEquals(newState, order.getState());

    }


    @Test
    public void NullOrderTest() {
        ShoppingCart cart1 = new ShoppingCart();
        String name1 = null;
        String address1 = null;
        int state1 = 0;

        shop.Order nullOrder = new Order(cart1, name1, address1, state1);

        assertNull(nullOrder.getCustomerAddress());
        assertNull(nullOrder.getCustomerName());
    }



    @Test
    public void removeItemFromShoppingCart_test() {

        int expectedCount = 1;
        cart.removeItem(2);
        int actualCount = cart.getItemsCount();
        assertEquals(expectedCount, actualCount);


        expectedCount = 0;
        cart.removeItem(1);
        actualCount = cart.getItemsCount();
        assertEquals(expectedCount, actualCount);

    }
}