package storage;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import shop.Item;
import shop.StandardItem;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {
    int id;
    String name;
    float price;
    String category;
    int lp;
    Item item;
    ItemStock itemStock;
    @BeforeEach
    void init(){
        id = 1;
        name = "Yaught";
        price = 10000;
        category = "Ships";
        lp = 1;
        item = new StandardItem(id,name,price,category, lp);
        itemStock = new ItemStock(item);
    }
    @Test
    void ItemStockTest(){

        assertEquals(item, itemStock.getItem());
        assertEquals(0, itemStock.getCount());

    }
    @Test
    void increaseItemCountTest(){
        int x = 1;
        itemStock.IncreaseItemCount(x);
        assertEquals(1, itemStock.getCount());
    }

    @Test
    void decreaseItemCountTest(){
        int x = 1;
        itemStock.decreaseItemCount(x);
        assertEquals(-1, itemStock.getCount());
    }

    @Test
    public void toStringTest() {
        int x = 1;
        itemStock.IncreaseItemCount(x);

        String expected = "STOCK OF ITEM:  " + item.toString() + "    PIECES IN STORAGE: 1";
        String actual = itemStock.toString();
        assertEquals(expected, actual);
    }
}