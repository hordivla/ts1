package archive;


import org.junit.jupiter.api.*;

import org.mockito.internal.matchers.Or;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class PurchasesArchiveTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;
    PurchasesArchive purchasesArchive;

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void printItemPurchaseStatisticsTest() {
        purchasesArchive = new PurchasesArchive();
        ShoppingCart cart = new ShoppingCart();
        Item item1 = new StandardItem(11, "GOW", 500, "game", 1);
        Item item2 = new StandardItem(22, "WOW", 250, "game", 2);
        cart.addItem(item1);
        cart.addItem(item2);

        String custName = "David";
        String custAddress = "Divad";
        Order order = new Order(cart, custName, custAddress, 1);
        purchasesArchive.putOrderToPurchasesArchive(order);
        String expectedOutput =
                "ITEM PURCHASE STATISTICS:\n" +
                        "ITEM  Item   ID 22   NAME WOW   CATEGORY game   PRICE 250.0   LOYALTY POINTS 2   HAS BEEN SOLD 1 TIMES\n" +
                        "ITEM  Item   ID 11   NAME GOW   CATEGORY game   PRICE 500.0   LOYALTY POINTS 1   HAS BEEN SOLD 1 TIMES\n";
        purchasesArchive.printItemPurchaseStatistics();
        Assertions.assertEquals(expectedOutput, outContent.toString());
    }
    @Test
    public void getHowManyTimesHasBeenItemSold_PutOrderToPurchasesArchiveTest() {
        purchasesArchive = new PurchasesArchive();
        ShoppingCart cart1 = new ShoppingCart();
        ShoppingCart cart2 = new ShoppingCart();
        Item item1 = new StandardItem(11, "GOW", 500, "game", 1);
        Item item2 = new StandardItem(22, "WOW", 250, "game", 2);
        cart1.addItem(item1);
        cart1.addItem(item2);
        cart2.addItem(item1);
        cart2.addItem(item2);

        String custName1 = "David";
        String custAddress1 = "Divad";
        String custName2 = "Liz";
        String custAddress2 = "Zil";
        Order order = new Order(cart1, custName1, custAddress1, 1);
        purchasesArchive.putOrderToPurchasesArchive(order);

        Assertions.assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(item1));
        Assertions.assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(item2));

        Order order2 = new Order(cart2, custName2, custAddress2, 2);

        purchasesArchive.putOrderToPurchasesArchive(order2);

        assertEquals(2, purchasesArchive.getHowManyTimesHasBeenItemSold(item1));
        assertEquals(2, purchasesArchive.getHowManyTimesHasBeenItemSold(item2));
    }


}