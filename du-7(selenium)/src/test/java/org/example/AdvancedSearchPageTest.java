package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import static org.junit.jupiter.api.Assertions.*;

class AdvancedSearchPageTest {
    private WebDriver driver;
    private AdvancedSearchPage advancedSearchPage;
    private String advancedSearchedPageURL = "https://link.springer.com/advanced-search";

    @BeforeEach
    public void setUp() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--whitelisted-ips=\"\"");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(options);
        advancedSearchPage = new AdvancedSearchPage(driver);
        PageFactory.initElements(driver, advancedSearchPage);
        driver.get(advancedSearchedPageURL);
        driver.manage().window().maximize();
    }

    


    @Test
    public void testAdvancedSearch() {
        advancedSearchPage.acceptCookie();
        advancedSearchPage.setAllWordsField("Page Object Model");
        advancedSearchPage.setLeastWordsField("Selenium Testing");
        advancedSearchPage.setStartYear("2023");
        advancedSearchPage.setEndYear("2023");
        advancedSearchPage.clickSearchButton();
        advancedSearchPage.clickChooseArticleButton();

        WebElement findResultElement =
                driver.findElement(By.xpath("//*[@id=\"number-of-search-results-and-search-terms\"]/strong[1]"));
        String countResultText = findResultElement.getText();
        assertEquals(countResultText, "1,634");
    }

    @Test
    public void testReadFirstArticle() {
        testAdvancedSearch();
        advancedSearchPage.clickReadFirstArticle();

        WebElement DOIElement1 =
                driver.findElement(By.xpath("//*[@id=\"article-info-content\"]/div/div[2]/ul[1]/li[4]/p/span[2]"));
        String expectedDOIText1 = DOIElement1.getText();
        String actualDOIText1 = "https://doi.org/10.1007/s11219-022-09583-4";
        assertEquals(expectedDOIText1, actualDOIText1);

        WebElement DateElement1 =
                driver.findElement(By.xpath("//*[@id=\"main-content\"]/main/article/div[1]/header/ul[1]/li/a/time"));
        String expectedDateText1 = DateElement1.getText();
        String actualDateText1 = "26 May 2022";
        assertEquals(expectedDateText1, actualDateText1);

        WebElement TitleElement1 =
                driver.findElement(By.xpath("//*[@id=\"main-content\"]/main/article/div[1]/header/h1"));
        String expectedTitleText1 = TitleElement1.getText();
        String actualTitleText1 = "Comparison of open-source runtime testing tools for microservices";
        assertEquals(expectedTitleText1, actualTitleText1);

    }

    @Test
    public void testReadSecondArticle() {
        testAdvancedSearch();
        advancedSearchPage.clickReadSecondArticle();

        WebElement DOIElement2 =
                driver.findElement(By.xpath("//*[@id=\"article-info-content\"]/div/div[2]/ul[1]/li[3]/p/span[2]"));
        String expectedDOIText2 = DOIElement2.getText();
        String actualDOIText2 = "https://doi.org/10.1007/s11219-023-09616-6";
        assertEquals(expectedDOIText2, actualDOIText2);

        WebElement DateElement2 =
                driver.findElement(By.xpath("//*[@id=\"main-content\"]/main/article/div[1]/header/ul[1]/li[2]/a/time"));
        String expectedDateText2 = DateElement2.getText();
        String actualDateText2 = "17 April 2023";
        assertEquals(expectedDateText2, actualDateText2);
        
        WebElement TitleElement2 =
                driver.findElement(By.xpath("//*[@id=\"main-content\"]/main/article/div[1]/header/h1"));
        String expectedTitleText2 = TitleElement2.getText();
        String actualTitleText2 = "MUTTA: a novel tool for E2E web mutation testing";
        assertEquals(expectedTitleText2, actualTitleText2);

    }

    @Test
    public void testReadThirdArticle() {
        testAdvancedSearch();
        advancedSearchPage.clickReadThirdArticle();

        WebElement DOIElement3 =
                driver.findElement(By.xpath("//*[@id=\"article-info-content\"]/div/div[2]/ul[1]/li[5]/p/span[2]"));
        String expectedDOIText3 = DOIElement3.getText();
        String actualDOIText3 = "https://doi.org/10.1007/s00521-023-08216-6";
        assertEquals(expectedDOIText3, actualDOIText3);

        WebElement DateElement3 =
                driver.findElement(By.xpath("//*[@id=\"main-content\"]/main/article/div[1]/header/ul[1]/li[2]/a/time"));
        String expectedDateText3 = DateElement3.getText();
        String actualDateText3 = "20 January 2023";
        assertEquals(expectedDateText3, actualDateText3);

        WebElement TitleElement3 =
                driver.findElement(By.xpath("//*[@id=\"main-content\"]/main/article/div[1]/header/h1"));
        String expectedTitleText3 = TitleElement3.getText();
        String actualTitleText3 = "A detector for page-level handwritten music object recognition based on deep learning";
        assertEquals(expectedTitleText3, actualTitleText3 );

    }


    @Test
    public void testReadFourthArticle() {
        testAdvancedSearch();
        advancedSearchPage.clickReadFourthArticle();

        WebElement DOIElement4 =
                driver.findElement(By.xpath("//*[@id=\"article-info-content\"]/div/div[2]/ul/li[4]/p/span[2]"));
        String expectedDOIText4 = DOIElement4.getText();
        String actualdDOIText4 = "https://doi.org/10.1186/s12951-022-01754-6";
        assertEquals(expectedDOIText4, actualdDOIText4);

        WebElement DateElement4 =
                driver.findElement(By.xpath("//*[@id=\"article-info-content\"]/div/div[2]/ul/li[3]/p/span[2]/time"));
        String expectedDateText4 = DateElement4.getText();
        String actualDateText4 = "09 February 2023";
        assertEquals(expectedDateText4, actualDateText4);


        WebElement TitleElement4 =
                driver.findElement(By.xpath("//*[@id=\"main-content\"]/main/article/div[1]/header/h1"));
        String expectedTitleText4 = TitleElement4.getText();
        String actualTitleText4 = "Protective effect of spore oil-functionalized nano-selenium system on cisplatin-induced nephrotoxicity by regulating oxidative stress-mediated pathways and activating immune response";
        assertEquals(expectedTitleText4, actualTitleText4);

    }


    @AfterEach
    public void quit() {
        driver.quit();
    }
}