package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;


import org.openqa.selenium.WebDriver;


import static org.junit.jupiter.api.Assertions.*;

class HomePageTest {
    private WebDriver driver;
    private HomePage homePage;
    private String homePageUrl = "https://link.springer.com/";

    @BeforeEach
    public void setUp() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--whitelisted-ips=\"\"");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(options);
        homePage = new HomePage(driver);
        PageFactory.initElements(driver, homePage);
        driver.get(homePageUrl);
        driver.manage().window().maximize();
    }


    @Test
    public void testLoginHomePage() {
        homePage.acceptCookie();
        homePage.clickLoginHomeButton();
        WebElement login = driver.findElement(By.xpath("//*[@id=\"content\"]/h1[1]"));
        String text = login.getText();
        assertEquals(text, "Welcome back. Please log in.");
    }


    @Test
    public void testAdvancedSearchPage() {
        homePage.acceptCookie();
        homePage.clickSettingButton();
        homePage.goToAdvancedSearch();
        WebElement searchButton = driver.findElement(By.xpath("//h1[text()='Advanced Search']"));
        String text = searchButton.getText();
        assertEquals(text, "Advanced Search");
    }


    @AfterEach
    public void quit() {
        driver.quit();
    }

}