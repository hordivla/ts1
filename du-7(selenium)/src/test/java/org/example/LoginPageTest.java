package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static org.junit.jupiter.api.Assertions.*;

class LoginPageTest {
    private WebDriver driver;
    private LoginPage loginPage;
    private String loginUrl = "https://link.springer.com/signup-login";


    @BeforeEach
    public void setUp() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--whitelisted-ips=\"\"");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(options);
        loginPage = new LoginPage(driver);
        PageFactory.initElements(driver, loginPage);
        driver.get(loginUrl);
        driver.manage().window().maximize();

    }


    @Test
    public void testLogin() {
        loginPage.acceptCookie();
        loginPage.setEmail("asdasd@asd.asd");
        loginPage.setPassword("asdasd123123");
        loginPage.clickLogin();
        WebElement userElement = driver.findElement(By.xpath("//*[@id='user']/span"));
        String userText = userElement.getText();
        assertEquals(userText, "David Sabov");
    }


    @AfterEach
    public void quit() {
        driver.quit();
    }

}