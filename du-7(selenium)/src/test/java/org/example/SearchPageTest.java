package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static org.junit.jupiter.api.Assertions.*;

class SearchPageTest {
    private WebDriver driver;
    private SearchPage searchPage;
    private String searchUrl = "https://link.springer.com/search";

    @BeforeEach
    public void setUp() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--whitelisted-ips=\"\"");

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(options);
        searchPage = new SearchPage(driver);
        PageFactory.initElements(driver, searchPage);
        driver.get(searchUrl);
        driver.manage().window().maximize();

    }





    @AfterEach
    public void quit() {
        driver.quit();
    }
}