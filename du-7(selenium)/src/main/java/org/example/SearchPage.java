package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPage {
    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"query\"]")
    private WebElement searchField;


    @FindBy(xpath = "//*[@id=\"search\"]")
    private WebElement searchButton;


    @FindBy(xpath = "/html/body/section/div/div[2]/button[1]")
    private WebElement acceptCookieButton;

    @FindBy(xpath = "//*[@id=\"results-list\"]/li[2]/div[2]/span[2]/a")
    private WebElement openFirstArticle;


    @FindBy(xpath = "//*[@id=\"results-list\"]/li/div[2]/span[2]/a")
    private WebElement openSecondArticle;


    @FindBy(xpath = "//*[@id=\"results-list\"]/li[1]/div[2]/span[2]/a")
    private WebElement openThirdArticle;


    @FindBy(xpath = "//*[@id=\"results-list\"]/li/div[2]/span[2]/a")
    private WebElement openFourthArticle;


    public SearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public void setSearch(String search) {
        searchField.sendKeys(search);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public void clickOpenFirstArticle() {
        openFirstArticle.click();
    }

    public void clickOpenSecondArticle() {
        openSecondArticle.click();
    }

    public void clickOpenThirdArticle() {
        openThirdArticle.click();
    }

    public void clickOpenFourthArticle() {
        openFourthArticle.click();
    }

    public void acceptCookie() {
        acceptCookieButton.click();
    }
}
