package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"login-box-email\"]")
    private WebElement emailField;

    @FindBy(xpath = "//*[@id=\"login-box-pw\"]")
    private WebElement passwordField;

    @FindBy(xpath = "//*[@id=\"login-box\"]/div/div[3]/button")
    private WebElement loginButton;

    @FindBy(xpath = "/html/body/section/div/div[2]/button[1]")
    private WebElement acceptCookieButton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void setEmail(String email) {
        emailField.sendKeys(email);
    }

    public void setPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void clickLogin() {
        loginButton.click();
    }

    public void acceptCookie() {
        acceptCookieButton.click();
    }
}
