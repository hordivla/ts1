package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvancedSearchPage {
    private WebDriver driver;


    @FindBy(xpath = "//*[@id=\"all-words\"]")
    private WebElement allWordsField;


    @FindBy(xpath = "//*[@id=\"least-words\"]")
    private WebElement leastWordsField;


    @FindBy(xpath = "//*[@id=\"facet-start-year\"]")
    private WebElement startYear;


    @FindBy(xpath = "//*[@id=\"facet-end-year\"]")
    private WebElement endYear;


    @FindBy(xpath = "//*[@id=\"submit-advanced-search\"]")
    private WebElement searchAdvButton;


    @FindBy(xpath = "//*[@id=\"content-type-facet\"]/ol/li[3]/a")
    private WebElement chooseArticleButton;


    @FindBy(xpath = "//*[@id=\"results-list\"]/li[1]/h2/a")
    private WebElement readFirstArticle;


    @FindBy(xpath = "//*[@id=\"results-list\"]/li[2]/div[2]/span[2]/a")
    private WebElement readSecondArticle;


    @FindBy(xpath = "//*[@id=\"results-list\"]/li[3]/div[2]/span[2]/a")
    private WebElement readThirdArticle;


    @FindBy(xpath = "//*[@id=\"results-list\"]/li[4]/div[2]/span[2]/a")
    private WebElement readFourthArticle;


    @FindBy(xpath = "/html/body/section/div/div[2]/button[1]")
    private WebElement acceptCookieButton;


    public AdvancedSearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void setAllWordsField(String email) {
        allWordsField.sendKeys(email);
    }

    public void setLeastWordsField(String password) {
        leastWordsField.sendKeys(password);
    }

    public void setStartYear(String start_year) {
        startYear.sendKeys(start_year);
    }

    public void setEndYear(String end_year) {
        endYear.sendKeys(end_year);
    }

    public void clickSearchButton() {
        searchAdvButton.click();
    }

    public void clickChooseArticleButton() {
        chooseArticleButton.click();
    }

    public void clickReadFirstArticle() {
        readFirstArticle.click();
    }

    public void clickReadSecondArticle() {
        readSecondArticle.click();
    }

    public void clickReadThirdArticle() {
        readThirdArticle.click();
    }

    public void clickReadFourthArticle() {
        readFourthArticle.click();
    }

    public void acceptCookie() {
        acceptCookieButton.click();
    }
}
