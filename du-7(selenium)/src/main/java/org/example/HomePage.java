package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    private WebDriver driver;


    @FindBy(xpath = "//*[@id=\"header\"]/div[1]/div[1]/a")
    private WebElement loginHomeButton;


    @FindBy(xpath = "//*[@id=\"search-options\"]/button")
    private WebElement settingButton;


    @FindBy(xpath = "//*[@id=\"advanced-search-link\"]")
    private WebElement advancedSearchLink;


    @FindBy(xpath = "//*[@id=\"home-page\"]/section/div/div[2]/button[1]")
    private WebElement acceptCookieButton;


    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickLoginHomeButton() {
        loginHomeButton.click();
    }

    public void clickSettingButton() {
        settingButton.click();
    }

    public void goToAdvancedSearch() {
        advancedSearchLink.click();
    }

    public void acceptCookie() {
        acceptCookieButton.click();
    }
}
